import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Handler
{
    private final static Handler INSTANCE = new Handler();

    List<MonitoredData> activities;

    private Handler()
    {
        activities = this.loadActivities("Activities.txt");
    }

    public static Handler getInstance()
    {
        return INSTANCE;
    }

    private List<MonitoredData> loadActivities(String path)
    {
        List<MonitoredData> data = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try
        {
            Files.lines(Paths.get(path)).forEach(e ->
            {
                String[] line = e.split("\\s{2}");
                try {
                    data.add(new MonitoredData(formatter.parse(line[0]), formatter.parse(line[1]), line[2].trim()));
                }
                catch (ParseException e1)
                {
                    e1.printStackTrace();
                }
            });
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return data;
    }

    public int countDays()
    {
        return (int) activities.parallelStream().map(MonitoredData::getStartDay).distinct().count();
    }

    public Map<String, Long> countActivitiesOccurences()
    {
       return activities.parallelStream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
    }

    public TreeMap<String, Long> countActivitiesByDay()
    {
        Map<String, Long> result = activities.stream().collect(Collectors.groupingBy(MonitoredData::dayAndActivity, Collectors.counting()));

        return new TreeMap<>(result);
    }

    public Map<String, Long> totalDurationsByActivity()
    {
        return activities.parallelStream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(MonitoredData::getDuration)));
    }

    public Map<String, Long> countActivitiesLessThan5Seconds()
    {
        return activities.parallelStream().filter(e -> e.getDuration() < 300).collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
    }

    public List<String> filteredActivities()
    {
        List<String> result = new ArrayList<>();
        TreeMap<String, Long> t1 = new TreeMap<>(this.countActivitiesOccurences());
        TreeMap<String, Long> t2 = new TreeMap<>(this.countActivitiesLessThan5Seconds());

        //traverse each
        Iterator<Map.Entry<String, Long>> it1 = t1.entrySet().iterator();

        while (it1.hasNext())
        {
            Map.Entry<String, Long> e1 = it1.next();

            if(t2.get(e1.getKey()) == null)
            {
                continue;
            }

            long other = t2.get(e1.getKey());

            float percentage = (other * 100f) / e1.getValue();

            if (percentage >= 90.0f) {
                    result.add(e1.getKey());
            }
        }

        return result;
    }

    public String printActivities()
    {
        StringBuilder sb = new StringBuilder();
        activities.forEach(e -> {
            sb.append(e.toString());
            sb.append("\n");
        });

        return sb.toString();
    }

    public String printActivitiesWithDuration()
    {
        StringBuilder sb1 = new StringBuilder();

        activities.forEach(e ->
        {
            StringBuilder sb = new StringBuilder();
            sb.append("start: " + e.getStartTime() + ", ");
            sb.append("end: " + e.getEndTime() + ", ");
            sb.append("activity: " + e.getActivity() + ", ");
            sb.append("duration: " + e.getDuration() + " seconds");

            sb1.append(sb.toString() + "\n");
        });

        return sb1.toString();
    }
}
