import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame
{
    private JTextArea textArea;
    private JButton refreshBtn;
    private JComboBox<String> options;

    public GUI()
    {
        String[] options = {"Activities", "Count Days", "Count Activities Occurrences", "Count Activities by Day", "Durations", "Total Durations by Activity", "Filtered Activities"};

        this.options = new JComboBox<>(options);
        this.textArea = new JTextArea(30, 70);
        this.refreshBtn = new JButton("Refresh");
        this.textArea.setEditable(false);

        JPanel panel = new JPanel();

        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(this.options);
        panel.add(new JScrollPane(this.textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
        panel.add(this.refreshBtn);

        this.setContentPane(panel);
        this.setVisible(true);
        this.setSize(850, 600);
        this.setResizable(false);
        this.setTitle("Activity processing");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.refreshBtn.addActionListener(e ->
        {
            switch (this.options.getSelectedIndex())
            {
                case 0:
                    this.textArea.setText(Handler.getInstance().printActivities());
                    break;
                case 1:
                    this.textArea.setText(Integer.toString(Handler.getInstance().countDays()));
                    break;
                case 2:
                    StringBuilder sb = new StringBuilder();
                    Handler.getInstance().countActivitiesOccurences().forEach((k, v) ->
                    {
                        sb.append(k + " " + v + "\n");
                    });
                    this.textArea.setText(sb.toString());
                    break;
                case 3:
                    StringBuilder sb1 = new StringBuilder();
                    Handler.getInstance().countActivitiesByDay().forEach((k, v) ->
                    {
                        sb1.append(k + " " + v + "\n");
                    });
                    this.textArea.setText(sb1.toString());
                    break;
                case 4:
                    this.textArea.setText(Handler.getInstance().printActivitiesWithDuration());
                    break;
                case 5:
                    StringBuilder sb2 = new StringBuilder();
                    Handler.getInstance().totalDurationsByActivity().forEach((k, v) ->
                    {
                        sb2.append(k + " " + v + " seconds\n");
                    });
                    this.textArea.setText(sb2.toString());
                    break;
                case 6:
                    StringBuilder sb3 = new StringBuilder();
                    Handler.getInstance().filteredActivities().forEach(e1 ->
                    {
                        sb3.append(e1 + "\n");
                    });
                    this.textArea.setText(sb3.toString());
                    default:
                        break;
            }
        });
    }
}
