import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonitoredData
{
    Date startTime;
    Date endTime;
    String activity;

    public MonitoredData(Date startTime, Date endTime, String activity)
    {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    public Date getStartDay()
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String formatted = format.format(this.startTime);

        Date result = null;

        try
        {
            result = format.parse(formatted);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    public long getDuration()
    {
        return (this.endTime.getTime() - this.startTime.getTime()) / 1000L;
    }

    public String dayAndActivity()
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(this.getStartDay()) + " " + this.activity;
    }

    @Override
    public String toString() {
        return  startTime +
                ", " + endTime +
                ", " + activity + '\n';
    }
}
